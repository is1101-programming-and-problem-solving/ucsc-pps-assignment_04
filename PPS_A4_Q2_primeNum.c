//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 04 - Question 02
//Write a program to determine the given number is a prime number.

#include <stdio.h>

int main () {

    int num, prime_count=2, temp_count=0;

    printf ("Enter number: ");
    scanf ("%d",&num);

    if (num<=0) {
        printf ("\nThis is not a prime number.\n");
    } else {
        for(int i=1; i<=num; i++) {
            if(num%i==0) {
                temp_count++;
            }
        }
        if (prime_count==temp_count) {
            printf ("\nThis is a prime number.\n");
        } else {
            printf ("\nThis is not a prime number.\n");
        }
    }

    return 0;

}
