//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 04 - Question 04
//Write a C program to find factors of a given number.

#include <stdio.h>

int main () {

    int num;

    printf ("Enter number: ");
    scanf ("%d",&num);

    for (int i=1; i<=num; i++) {
        if (num%i==0) {
            printf ("%d  ",i);
        }
    }
    printf ("\n");

    return 0;

}
