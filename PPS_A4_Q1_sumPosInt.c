//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 04 - Question 01
//Write a program to calculate the sum of all given positive integers until we enter the 0 or a negative value.

#include <stdio.h>

int main () {

    int num, sum=0;

    printf ("Enter number: ");
    scanf ("%d",&num);

    while(num>0) {
        sum+=num;
        printf ("Enter number: ");
        scanf ("%d",&num);
    }

    printf ("\nTotal is %d\n",sum);

    return 0;

}
