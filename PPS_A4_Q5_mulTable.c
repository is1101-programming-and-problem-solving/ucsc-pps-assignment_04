//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 04 - Question 05
//By given an integer value n Write a C Program to print multiplication tables from 1 to n.

#include <stdio.h>

int main () {

    int num;

    printf ("Enter number: ");
    scanf ("%d",&num);
    printf ("\n");

    for (int i=1; i<=12; i++) {
        for (int j=1; j<=num; j++) {
            printf ("%dx%d=%d\t",j,i,i*j);
        }
        printf ("\n");
    }

    return 0;

}
