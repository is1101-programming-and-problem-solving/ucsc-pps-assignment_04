//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 04 - Question 03
//Write a C Program to Reverse a Number.

#include <stdio.h>

int main () {

    int num, rev=0, rem=0;

    printf ("Enter number: ");
    scanf ("%d",&num);

    while(num>0) {
        rem=num%10;
        rev=rev*10+rem;
        num/=10;
    }

    printf ("Reversed number: %d\n",rev);

    return 0;

}
