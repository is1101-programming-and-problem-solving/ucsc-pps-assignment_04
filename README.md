# UCSC PPS Assignment_04

1) [Write a  program to calculate the sum of all given positive integers until we enter the 0 or a negative value.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_04/-/blob/main/PPS_A4_Q1_sumPosInt.c)

2) [Write a program to determine the given number is a prime number.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_04/-/blob/main/PPS_A4_Q2_primeNum.c)

3) [Write a C Program to Reverse a number.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_04/-/blob/main/PPS_A4_Q3_revNum.c)

4) [Write a C program to find factors of a given number.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_04/-/blob/main/PPS_A4_Q4_factors.c)

5) [By given an integer value n Write a C Program to print multiplication tables from 1 to n.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_04/-/blob/main/PPS_A4_Q5_mulTable.c)
